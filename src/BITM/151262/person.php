<?php

namespace app;


class Person
{
    public $name = "karim";
    public $gender = "male";
    public $blood_group = "AB+";

    public function showPersonInfo(){
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";

    }

}